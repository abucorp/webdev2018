// Complete the below questions using this array:
const array = [
  {
    username: "john",
    team: "red",
    score: 5,
    items: ["ball", "book", "pen"]
  },
  {
    username: "becky",
    team: "blue",
    score: 10,
    items: ["tape", "backpack", "pen"]
  },
  {
    username: "susy",
    team: "red",
    score: 55,
    items: ["ball", "eraser", "pen"]
  },
  {
    username: "tyson",
    team: "green",
    score: 1,
    items: ["book", "pen"]
  },

];

//Create an array using forEach that has all the usernames with a "!" to each of the usernames
const newArray = [];
array.forEach((user)=>{
  user.username+='!';
  newArray.push(user);
});
console.log(newArray);

//Create an array using map that has all the usernames with a "? to each of the usernames
const arrayMap = array.map((user) => {
    user.username+='?';
    return user;
});
console.log(arrayMap);

//Filter the array to only include users who are on team: red
const teamRed = array.filter((user) => user.team === 'red');
console.log(teamRed);


//Find out the total score of all users using reduce
const totalScore = array.reduce((acc, user) => user.score + acc, 0);
console.log(totalScore);

// (1), what is the value of i?
// (2), Make this map function pure:
const arrayNum = [1, 2, 4, 5, 8, 9];
const bozoArray = arrayNum.map((num, i) => {
	return num * 2 + i;
});
console.log(bozoArray);


//BONUS: create a new list with all user information, but add "!" to the end of each items they own.
addExclamation = (s) => s + '!';
bonusArray = array.map((u) => {
  u.items = u.items.map(addExclamation);
  return u;
});
console.log(bonusArray);