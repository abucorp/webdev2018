var li = document.querySelectorAll("li");
for (var i = 0; i< li.length; i++) {
    addDeleteButtonAndClickListener(li[i]);
}

function addDeleteButtonAndClickListener(element){
    element.innerHTML += ' ';
    element.appendChild(createDeleteButtonElement());
    element.addEventListener('click', function (event) {
        event.target.classList.toggle('done');
    });
}

function createDeleteButtonElement() {
    var button = document.createElement('button');
    var text = document.createTextNode('Delete');
    button.appendChild(text);
    button.className= "btn btn-danger btn-sm right delete";
    button.addEventListener('click', function (event) {
        event.target.parentNode.parentNode.removeChild(event.target.parentNode);
    });
    return button;
}

